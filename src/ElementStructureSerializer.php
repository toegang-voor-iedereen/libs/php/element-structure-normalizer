<?php

declare(strict_types=1);

namespace NLdoc\ElementStructure\Serialization;

use NLdoc\ElementStructure\Types\Element\DocumentElement;
use Symfony\Component\PropertyInfo\Extractor\PhpDocExtractor;
use Symfony\Component\PropertyInfo\Extractor\ReflectionExtractor;
use Symfony\Component\PropertyInfo\PropertyInfoExtractor;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Normalizer\AbstractObjectNormalizer;
use Symfony\Component\Serializer\Normalizer\ArrayDenormalizer;
use Symfony\Component\Serializer\Normalizer\BackedEnumNormalizer;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Symfony\Component\Serializer\Serializer;
use Symfony\Component\Serializer\SerializerInterface;

/**
 * @method array getSupportedTypes(?string $format)
 */
class ElementStructureSerializer
{
    private SerializerInterface $serializer;

    public function __construct()
    {
        $this->serializer = new Serializer(
            [
                new BackedEnumNormalizer(),
                new ObjectNormalizer(
                    null,
                    null,
                    null,
                    new PropertyInfoExtractor(
                        [],
                        [
                            new PhpDocExtractor(),
                            new ReflectionExtractor()
                        ]
                    ),
                    new ElementStructureClassDiscriminatorResolver()
                ),
                new ArrayDenormalizer()
            ],
            [ new JsonEncoder() ]
        );
    }

    public function serialize(mixed $data): string
    {
        return $this->serializer->serialize(
            $data,
            'json',
            [
                AbstractObjectNormalizer::PRESERVE_EMPTY_OBJECTS => true
            ]
        );
    }

    public function deserialize(mixed $data): DocumentElement
    {
        return $this->serializer->deserialize(
            $data,
            DocumentElement::class,
            'json'
        );
    }
}
